/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import modules.tubesjava.Module_Frame;
import modules.Module_KeyBoard;
import modules.tubesjava.Module_FramePlayerController;
import ports.OutPort;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import modules.tubesjava.Module_FramePlayer_2;
import processing.core.*;
import tubesjava.IExecutable;
import tubesjava.MyKeyEvent;
import modules.tubesjava.Module_FramesRenderer;

/**
 *
 * @author Yousif
 */
public class P extends PApplet {

    public static PApplet P;
    public static ArrayList<IExecutable> execs;
    public OutPort<Integer> outTime;
    public OutPort<LinkedList<MyKeyEvent>> outKey;
    //
    Module_KeyBoard keyboard;
    Module_Frame frameMod;
    Module_FramePlayerController controller;
    Module_FramePlayer_2 player;
    Module_FramesRenderer framesRenderer;
    //
    static int SCREEN_WIDTH = 0;
    static int SCREEN_HEIGHT = 0;
    static boolean isFullScreen = true;

    public static void main(String args[]) {
        if (isFullScreen) {
            PApplet.main(new String[]{"--present", "main.P"});
        } else {
            PApplet.main(new String[]{"main.P"});
        }
    }

    public void setup() {
        P = this;
        if (isFullScreen) {
            size(displayWidth, displayHeight, P3D);
        } else {
//            size(1024, 600, P3D);
            size(512, 300, P3D);
        }
        background(0);

        outTime = new OutPort<>(millis());
        outKey = new OutPort<>(new LinkedList<MyKeyEvent>());

        keyboard = new Module_KeyBoard();
        frameMod = new Module_Frame(0x00FFFF, 10);
        controller = new Module_FramePlayerController(KeyEvent.VK_Q, KeyEvent.VK_W, KeyEvent.VK_E, null);
        player = new Module_FramePlayer_2();
        framesRenderer = new Module_FramesRenderer();

        execs = new ArrayList<>();
        execs.add(keyboard);
        execs.add(controller);
        execs.add(frameMod);
        execs.add(player);
        execs.add(framesRenderer);
        player.inCreationType.connectTo(controller.outType);
        player.inTime.connectTo(outTime);
        player.inTube.connectTo(frameMod.outFrame);
        controller.keyMapping.connectTo(keyboard.keyMapping);
        keyboard.inKey.connectTo(outKey);
        framesRenderer.inTubes.connectTo(player.outTubes);
    }

    public void update() {
        outTime.set(millis());
        int len = execs.size();
        for (int i = 0; i < len; i++) {
            execs.get(i).execute();
        }
//        outKey.set(null);
    }
static int fcount = 0;
    @Override
    public void keyPressed() {
        System.out.println(fcount + " " + keyCode + " down");
        outKey.get().add(new MyKeyEvent(KeyEvent.KEY_PRESSED, keyCode));
        
    }

    @Override
    public void keyReleased() {
        System.out.println(fcount + " " + keyCode + " up");
        outKey.get().add(new MyKeyEvent(KeyEvent.KEY_RELEASED, keyCode));
    }

    @Override
    public void draw() {
        fcount++;
        background(0);

        noCursor();
        updateMouse();
        showFrameRate();
        showInfo();
        
        float aspectRatio = ((float) P.width) / P.height;
        
//        perpective stuff
        float fov = P.PI / 3.0f;
        float cameraZ = (height / 2.0f) / tan(fov / 2.0f);
        perspective(fov, aspectRatio, cameraZ / 10.0f, cameraZ * 10.0f);
        
        //ortho stuff
//        float viewSize = 3000.0f;
//        ortho(0, width, 0,height, -2000,2000);
        
        camera(((mouseX * 1.0f / width) - 0.5f) * 3000.0f, //eye x
                ((mouseY * 1.0f / height) - 0.5f) * 3000.0f,//eye y
                (height / 2) / tan(PI / 6), //eye z
                0,0,-2100,//width / 2, height / 2, -4500, //centre x y z
                0, 1, 0);//up x y z);//up x y z

        update();
        drawAxis(200);
    }

    void showFrameRate() {
        camera(width / 2, height / 2, (height / 2) / tan(PI / 6), width / 2, height / 2, 0, 0, 1, 0);
        fill(255);
        text(frameRate, 20, 20);
    }
    
    void showInfo() {
        camera(width / 2, height / 2, (height / 2) / tan(PI / 6), width / 2, height / 2, 0, 0, 1, 0);
        fill(255);
        text("Size: " + Module_FramePlayer_2.GLOBALSIZE, 20, 40);
    }

    void updateMouse() {
        camera(width / 2, height / 2, (height / 2) / tan(PI / 6), width / 2, height / 2, 0, 0, 1, 0);
        noStroke();
        fill(255, 255, 255, 50);
        rect(mouseX-2, mouseY -2, 4, 4);
    }

    private void drawAxis(int length) {
        
//        float fov = P.PI / 3.0f;
//        float cameraZ = (height / 2.0f) / tan(fov / 2.0f);
//        perspective(fov, ((float) P.width) / P.height, cameraZ / 10.0f, cameraZ * 10.0f);
        stroke(255, 0, 0);
        fill(255,0,0);
        line(-length/2, 0, 0, length/2, 0, 0);//xaxis
        translate(length/2, 0, 0);
        box(10,10,10);
        translate(-length/2, 0, 0);
        
        stroke(0, 255, 0);
        fill(0,255,0);
        line(0, -length/2, 0, 0, length/2, 0);//yaxis
        translate(0, length/2, 0);
        box(10,10,10);
        translate(0, -length/2, 0);
        
        stroke(0, 0, 255);
        fill(0,0,255);
        line(0, 0, -length/2, 0, 0, length/2);//zaxis
        translate(0, 0, length/2);
        box(10,10,10);
        translate(0, 0, -length/2);
        
    }

}
