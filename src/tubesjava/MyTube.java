/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesjava;

import main.P;
import processing.core.PApplet;

/**
 *
 * @author Yousif
 */
public class MyTube {

    public Integer color;
    public MyCircle front;
    public MyCircle back;
    public MyRect[] rects;
    public Boolean clamped = false;
    PApplet p;
    public static MyTube defaultTube = new MyTube(
            new MyCircle(new Point(P.P.width / 2, P.P.height / 2, 0), 200.0f, 8),
            new MyCircle(new Point(P.P.width / 2, P.P.height / 2, -700), 200.0f, 8));

    public MyTube(MyCircle circle1, MyCircle circle2) {
        this.front = circle1;
        this.back = circle2;
        rects = new MyRect[circle1.points.length];
        createRects(circle1, circle2);
    }

    public MyTube(MyCircle circle1, MyCircle circle2, MyRect[] rects) {
        this.front = circle1;
        this.back = circle2;
        copyRects(rects);
    }
    
    public MyTube(MyCircle circle1, MyCircle circle2, Integer c) {
        this.front = circle1;
        this.back = circle2;
        this.color = c;
        rects = new MyRect[circle1.points.length];
        createRects(circle1, circle2);
    }

    void createRects(MyCircle circle1, MyCircle circle2) {
        int len = circle1.points.length;
        for (int i = 0; i < len; i++) {
            rects[i] = new MyRect(circle1.points[i], circle1.points[(i + 1) % len], circle2.points[(i + 1) % len], circle2.points[i]);
        }
    }

    void copyRects(MyRect[] rects) {
    }

    public void draw() {
        if (color != null) {
            P.P.fill(color);
            for (MyRect r : rects) {
                P.P.shape(r.shape);
            }
        } else {
            P.P.fill(0xFFFFFFFF);
            for (MyRect r : rects) {
                P.P.shape(r.shape);
            }
        }
    }
}