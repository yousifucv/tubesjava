/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesjava;

import main.P;

/**
 *
 * @author Yousif
 */
public class MyCircle {

    public Point[] points;
    static float PI = (float) Math.PI;
    static Point[] cachedPoints;
    static int cachedSegments = 0;

    static float cos(float a) {
        return (float) Math.cos(a);
    }

    static float sin(float a) {
        return (float) Math.sin(a);
    }

    private MyCircle() {
    }

    public MyCircle(Point centre, float radius, int segments) {
        if (cachedSegments == segments) {
            this.points = new Point[segments];
            //need the zDiff all the circles will overlap
            //this is bad and broken and needs to be rethought
            float zDiff = centre.z;
            copyPointsNewZ(cachedPoints, points, zDiff);
        } else {
            cachedPoints = new Point[segments];
            points = new Point[segments];
            float adjustmentAngle = P.P.radians(0);
            float oneAngle = 2 * PI / segments;
            for (int i = 0; i < segments; i++) {
                float angle = oneAngle * i - adjustmentAngle;
//                if (angle >= 0 && angle <= PI / 2) { //for some reason i thought all the rest were neccesary
                cachedPoints[i] = new Point(radius * cos(angle) + centre.x, radius * sin(angle) + centre.y, centre.z);
//                } else if (angle > PI / 2 && angle <= PI) {
//                    cachedPoints[i] = new Point(centre.x - radius * cos(PI - angle), radius * sin(PI - angle) + centre.y, centre.z);
//                } else if (angle > PI && angle <= 3 * PI / 2) {
//                    cachedPoints[i] = new Point(centre.x - radius * cos(angle - PI), centre.y - radius * sin(angle - PI), centre.z);
//                } else {
//                    cachedPoints[i] = new Point(centre.x + radius * cos(2 * PI - angle), centre.y - radius * sin(2 * PI - angle), centre.z);
//                }
            }
            this.points = new Point[segments];
            //need the zDiff all the circles will overlap
            //this is bad and broken and needs to be rethought
            float zDiff = centre.z;
            copyPointsNewZ(cachedPoints, points, zDiff);
            cachedSegments = segments;
        }

    }

    public static void copyPoints(Point[] src, Point[] dest) {
        for (int i = 0; i < src.length; i++) {
            dest[i] = new Point(src[i].x, src[i].y, src[i].z);
        }
    }

    public static void copyPointsNewZ(Point[] src, Point[] dest, float nz) {
        for (int i = 0; i < src.length; i++) {
            dest[i] = new Point(src[i].x, src[i].y, nz);
        }
    }

    public static void copyPointsNewZ_AngleAdjusted(Point[] src, Point[] dest, float nz, float angleAdj) {
        for (int i = 0; i < src.length; i++) {
            dest[i] = new Point(src[i].x, src[i].y, nz);
        }
    }

    public static MyCircle copyCircle(MyCircle src) {
        MyCircle ret = new MyCircle();
        ret.points = new Point[src.points.length];
        copyPoints(src.points, ret.points);
        return ret;
    }

    public static MyCircle copyCircleNewZ(MyCircle src, float nz) {
        MyCircle ret = new MyCircle();
        ret.points = new Point[src.points.length];
        copyPointsNewZ(src.points, ret.points, nz);
        return ret;
    }
}
