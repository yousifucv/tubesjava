/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tubesjava;

import main.P;
import modules.tubesjava.Module_FramePlayer_2;
import processing.core.PApplet;
import processing.core.PShape;

/**
 *
 * @author Yousif
 */
public class MyRect {
  public PShape shape;
  public Point location; //last point location
  static PApplet p = P.P;
  

  MyRect(Point p1, Point p2, Point p3, Point p4) {
    location = new Point(p4.x, p4.y, p4.z);
    
    shape = p.createShape();
    shape.setFill(true);
    shape.setFill(Module_FramePlayer_2.newColor);
    shape.setStrokeWeight(2);
    shape.setStroke(0xFFFFFFFF);
    shape.setStroke(true);
    shape.beginShape();
    shape.vertex(p1.x, p1.y, p1.z);
    shape.vertex(p2.x, p2.y, p2.z);
    shape.vertex(p3.x, p3.y, p3.z);
    shape.vertex(p4.x, p4.y, p4.z);
    shape.endShape(P.CLOSE);
  }
}
