/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modules.tubesjava;

import ports.InPort;
import ports.OutPort;
import java.util.ArrayList;
import tubesjava.IExecutable;

/**
 *
 * @author Yousif
 */
public class Module_FramePlayerController  implements IExecutable {

    public InPort<Integer> singleKey;
    public InPort<Integer> continiousKey;
    public InPort<Integer> repeatedKey;
    public InPort<ArrayList<Boolean>> keyMapping;
    public OutPort<Module_FramePlayer_2.TubeCreationType> outType;

    public Module_FramePlayerController(int single, int contin, int rep, ArrayList<Boolean> keys) {
        singleKey = new InPort<>(single);
        continiousKey = new InPort<>(contin);
        repeatedKey = new InPort<>(rep);
        keyMapping = new InPort<>(keys);
        outType = new OutPort<>(Module_FramePlayer_2.TubeCreationType.NONE);
    }

    public void execute() {
        if (keyMapping.get().get(singleKey.get()) == Boolean.TRUE) {
            outType.set(Module_FramePlayer_2.TubeCreationType.SINGLE);
        } else if (keyMapping.get().get(continiousKey.get()) == Boolean.TRUE) {
            outType.set(Module_FramePlayer_2.TubeCreationType.CONTINIOUS);
        } else if (keyMapping.get().get(repeatedKey.get()) == Boolean.TRUE) {
            outType.set(Module_FramePlayer_2.TubeCreationType.REPEATED);
        } else {
            outType.set(Module_FramePlayer_2.TubeCreationType.NONE);
        }
    }
}
