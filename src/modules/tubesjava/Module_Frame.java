/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modules.tubesjava;

import ports.InPort;
import ports.OutPort;
import tubesjava.IExecutable;
import tubesjava.MyCircle;
import tubesjava.MyTube;
import main.P;
import tubesjava.Point;

/**
 *
 * @author Yousif
 */
public class Module_Frame implements IExecutable {

    public InPort<Integer> color;
    public InPort<Integer> thickness;
    public OutPort<MyTube> outFrame;

    public Module_Frame(int color, int thick) {
        this.color = new InPort<>(new Integer(0));
        this.thickness = new InPort<>(new Integer(0));

        this.color.set(color);
        this.thickness.set(thick);

        MyCircle c1 = new MyCircle(new Point(0,0, -2000), 200.0f, 6);//P.P.width / 2, P.P.height / 2, -2000), 200.0f, 6);
        MyCircle c2 = new MyCircle(new Point(0,0, -2100), 200.0f, 6);//P.P.width / 2, P.P.height / 2, -2100), 200.0f, 6);
        
        this.outFrame = new OutPort<>(new MyTube(c1, c2));
    }

    @Override
    public void execute() {
    }
}
