/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modules.tubesjava;

import ports.InPort;
import tubesjava.IExecutable;
import tubesjava.MyTube;
import vizcollections.VizLinkedList;

/**
 *
 * @author Yousif
 */
public class Module_FramesRenderer implements IExecutable {
        //inputs

    public InPort<VizLinkedList<MyTube>> inTubes;
    //outputs
    
    //control values

    public Module_FramesRenderer() {
        inTubes = new InPort<>(new VizLinkedList<MyTube>());
        execute();
    }

    @Override
    public void execute() {
        draw();
    }

    public void draw() {
        VizLinkedList<MyTube> tubes = inTubes.get();
        VizLinkedList<MyTube>.Node<MyTube> tube = tubes.firstNode;
//        for (tube = tubes.firstNode; tube != null; tube = tube.next()) {
//            tube.getItem().draw();
//        }
        while(tube != null) {
            tube.getItem().draw();
            tube = tube.next();
        }
    }
}
