/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modules.tubesjava;

import ports.InPort;
import ports.OutPort;
import processing.core.PVector;
import tubesjava.IExecutable;
import tubesjava.MyCircle;
import tubesjava.MyTube;
import main.P;
import vizcollections.VizLinkedList;

/**
 * Uses VIZLinkedList
 *
 * @author Yousif
 */
public class Module_FramePlayer_2 implements IExecutable {

    //inputs
    public InPort<MyTube> inTube;
    public InPort<TubeCreationType> inCreationType;
    public InPort<Integer> inRepeatMin;
    public InPort<Integer> inTime;
    //outputs
    public OutPort<VizLinkedList<MyTube>> outTubes;
    //control values
    TubeCreationType prevType = TubeCreationType.NONE;
    Integer prevRepeatTime;
    Integer prevManageTime;
    float speed = 2000.00f;
    float maxZ = 700.0f;
    //globals
    public static int GLOBALSIZE = 0;
//    public static int[] randColors = {0xFFCCCCB2, 0xFFA37547,   0xFFD0D094,0xFF94704D}; //brown, earth tones
//    public static int[] randColors = {0xFFFF33CC, 0xFFCCFFFF, 0xFF66FF33, 0xFFFFFF00};
    public static int[] randColors = {0xFFFF0000, 0xFFc7c7c7, 0xFFAAAAAA, 0xffbaadad, 0xfff0eeee, 0xff9b6161, 0xffba0000};//red white grey
//    public static int[] randColors = {0xFFec008c, 0xFFa864a8, 0xFFf5989d, 0xFFbd8cbf};//purples and pinks
    public static int randCounter = 0;
    public static int newColor = randColors[randCounter++ % randColors.length];

    public Module_FramePlayer_2() {

        inTube = new InPort<>(MyTube.defaultTube);
        inRepeatMin = new InPort<>(100);
        inCreationType = new InPort<>(TubeCreationType.NONE);
        inTime = new InPort<>(P.P.millis());
        outTubes = new OutPort<>(new VizLinkedList<MyTube>());

        prevRepeatTime = inTime.get();
        prevManageTime = inTime.get();
        execute();
    }

    @Override
    public void execute() {
        manage();
        if (outTubes.get().size() > 0 && inCreationType.get() != TubeCreationType.CONTINIOUS && prevType == TubeCreationType.CONTINIOUS) {
            outTubes.get().getLast().clamped = false;
        }
        if (inCreationType.get() == TubeCreationType.NONE) {
            prevType = TubeCreationType.NONE;
        } else if (inCreationType.get() == TubeCreationType.SINGLE && prevType != TubeCreationType.SINGLE) {
            float nz = inTube.get().back.points[0].z;
            outTubes.get().add(new MyTube(MyCircle.copyCircle(inTube.get().front), MyCircle.copyCircleNewZ(inTube.get().back, nz)));
            prevType = TubeCreationType.SINGLE;
            newColor = randColors[randCounter++ % randColors.length];
        } else if (inCreationType.get() == TubeCreationType.CONTINIOUS && prevType != TubeCreationType.CONTINIOUS) {
            float nz = inTube.get().back.points[0].z;
            outTubes.get().add(new MyTube(MyCircle.copyCircle(inTube.get().front), MyCircle.copyCircleNewZ(inTube.get().back, nz)));
            outTubes.get().getLast().clamped = true;
            prevType = TubeCreationType.CONTINIOUS;
            newColor = randColors[randCounter++ % randColors.length];
        } else if (inCreationType.get() == TubeCreationType.CONTINIOUS && prevType == TubeCreationType.CONTINIOUS) {
            prevType = TubeCreationType.CONTINIOUS;
        } else if (inCreationType.get() == TubeCreationType.REPEATED) {
            if ((inTime.get() - prevRepeatTime) >= inRepeatMin.get()) {
                float nz = inTube.get().back.points[0].z;
                outTubes.get().add(new MyTube(MyCircle.copyCircle(inTube.get().front), MyCircle.copyCircleNewZ(inTube.get().back, nz)));
                
                prevRepeatTime = inTime.get();
                newColor = randColors[randCounter++ % randColors.length];
            }
            prevType = TubeCreationType.REPEATED;
        }
    }

    private void manage() {
        VizLinkedList<MyTube> tubes = outTubes.get();
        VizLinkedList<MyTube>.Node<MyTube> tubeNode = tubes.firstNode;
//        for (tubeNode = tubes.firstNode; tubeNode != null; tubeNode = tubeNode.next()) {
        while (tubeNode != null) {
            MyTube tube = tubeNode.getItem();
            if (tube.rects[0].location.z >= maxZ) {
                VizLinkedList<MyTube>.Node<MyTube> temp = tubeNode.next();
                tubes.remove(tubeNode);
                tubeNode = temp;
                continue;
            }
            if (tube.clamped) {
                for (int j = 0; j < tube.rects.length; j++) {
                    PVector v0 = tube.rects[j].shape.getVertex(0);
                    PVector v1 = tube.rects[j].shape.getVertex(1);
                    v0.z += speed * (inTime.get() - prevManageTime) / 1000.0f;
                    v1.z += speed * (inTime.get() - prevManageTime) / 1000.0f;
                    tube.rects[j].shape.setVertex(0, v0);
                    tube.rects[j].shape.setVertex(1, v1);
                }
            } else {
                for (int j = 0; j < tube.rects.length; j++) {

                     //translating creates a weird stroke artifact, so i have to resort to the calls below
//                     tube.rects[j].shape.translate(0, 0, speed * (inTime.get() - prevManageTime) / 1000.0f);
                    PVector v0 = tube.rects[j].shape.getVertex(0);
                    PVector v1 = tube.rects[j].shape.getVertex(1);
                    PVector v2 = tube.rects[j].shape.getVertex(2);
                    PVector v3 = tube.rects[j].shape.getVertex(3);
                    v0.z += speed * (inTime.get() - prevManageTime) / 1000.0f;
                    v1.z += speed * (inTime.get() - prevManageTime) / 1000.0f;
                    v2.z += speed * (inTime.get() - prevManageTime) / 1000.0f;
                    v3.z += speed * (inTime.get() - prevManageTime) / 1000.0f;
                    tube.rects[j].shape.setVertex(0, v0);
                    tube.rects[j].shape.setVertex(1, v1);
                    tube.rects[j].shape.setVertex(2, v2);
                    tube.rects[j].shape.setVertex(3, v3);
                    //updating the z marked location of this vertex
                    //this is getting silly, just for removal detection
                    tube.rects[j].location.z += speed * (inTime.get() - prevManageTime) / 1000.0f;

                }
            }
            tubeNode = tubeNode.next();
        }
        GLOBALSIZE = tubes.size();
        prevManageTime = inTime.get();

    }

//    public void draw() {
//        VizLinkedList<MyTube> tubes = outTubes.get();
//        VizLinkedList<MyTube>.Node<MyTube> tube = tubes.firstNode;
////        for (tube = tubes.firstNode; tube != null; tube = tube.next()) {
////            tube.getItem().draw();
////        }
//        while(tube != null) {
//            tube.getItem().draw();
//            tube = tube.next();
//        }
//    }
    public static enum TubeCreationType {

        SINGLE, CONTINIOUS, REPEATED, NONE;
    }
}
