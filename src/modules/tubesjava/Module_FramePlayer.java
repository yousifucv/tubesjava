package modules.tubesjava;

///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package modules;
//
//import ports.InPort;
//import ports.OutPort;
//import java.util.ArrayList;
//import processing.core.PVector;
//import tubesjava.IExecutable;
//import tubesjava.MyCircle;
//import tubesjava.MyTube;
//import tubesjava.P;
//
///**
// *
// * @author Yousif
// */
//public class Module_FramePlayer implements IExecutable {
//    
////inputs
//    public InPort<MyTube> inTube;
//    public InPort<TubeCreationType> inCreationType;
//    public InPort<Integer> inRepeatMin;
//    public InPort<Integer> inTime;
//    //outputs
//    OutPort<MyTube[]> outTubes;
//    //
//    //control values
//    TubeCreationType prevType = TubeCreationType.NONE;
//    Integer prevRepeatTime;
//    Integer prevManageTime;
//    MyTube[] internalTubesArray;
//    int tubeArrayCount;
//    float speed = 2000.00f;
//    float maxZ = 700.0f;
////    public static int[] randColors = {0xFFCCCCB2, 0xFFA37547,   0xFFD0D094,0xFF94704D};
////    public static int[] randColors = {0xFFFF33CC, 0xFFCCFFFF, 0xFF66FF33, 0xFFFFFF00};
//    public static int[] randColors = {0xFFFF0000, 0xFFFFFFFF, 0xFFDDDDDD};
//    public static int randCounter = 0;
//    public static int newColor = randColors[randCounter++ % randColors.length];
//
//    public Module_FramePlayer() {
//        internalTubesArray = new MyTube[128];
//        tubeArrayCount = 0;
//        inTube = new InPort<>(MyTube.defaultTube);
//        inRepeatMin = new InPort<>(50);
//        inCreationType = new InPort<>(TubeCreationType.NONE);
//        inTime = new InPort<>(P.P.millis());
//        outTubes = new OutPort<>(new LinkedList<MyTube>());
//
//
//        prevRepeatTime = inTime.get();
//        prevManageTime = inTime.get();
//        execute();
//    }
//
//    @Override
//    public void execute() {
//        manage();
//        if (inCreationType.get() == TubeCreationType.NONE) {
//            if (prevType == TubeCreationType.CONTINIOUS) {
//                outTubes.get().get(outTubes.get().size() - 1).clamped = false;
//            }
//            prevType = TubeCreationType.NONE;
//        } else if (inCreationType.get() == TubeCreationType.SINGLE && prevType != TubeCreationType.SINGLE) {
//            float nz = inTube.get().back.points[0].z;
//            outTubes.get().add(new MyTube(MyCircle.copyCircle(inTube.get().front), MyCircle.copyCircleNewZ(inTube.get().back, nz)));
//            prevType = TubeCreationType.SINGLE;
//            newColor = randColors[randCounter++ % randColors.length];
//        } else if (inCreationType.get() == TubeCreationType.CONTINIOUS && prevType != TubeCreationType.CONTINIOUS) {
//            float nz = inTube.get().back.points[0].z;
//            outTubes.get().add(new MyTube(MyCircle.copyCircle(inTube.get().front), MyCircle.copyCircleNewZ(inTube.get().back, nz)));
//            outTubes.get().get(outTubes.get().size() - 1).clamped = true;
//            prevType = TubeCreationType.CONTINIOUS;
//            newColor = randColors[randCounter++ % randColors.length];
//        } else if (inCreationType.get() == TubeCreationType.CONTINIOUS && prevType == TubeCreationType.CONTINIOUS) {
//            prevType = TubeCreationType.CONTINIOUS;
//        } else if (inCreationType.get() == TubeCreationType.REPEATED) {
//            if ((inTime.get() - prevRepeatTime) >= inRepeatMin.get()) {
//                float nz = inTube.get().back.points[0].z;
//                outTubes.get().add(new MyTube(MyCircle.copyCircle(inTube.get().front), MyCircle.copyCircleNewZ(inTube.get().back, nz)));
//                prevRepeatTime = inTime.get();
//                newColor = randColors[randCounter++ % randColors.length];
//            }
//            prevType = TubeCreationType.REPEATED;
//        }
//    }
//
//    private void manage() {
//        ArrayList<MyTube> tubes = outTubes.get();
//        int len = tubes.size();
//        for (int i = len - 1; i >= 0; i--) {
//            MyTube tube = tubes.get(i);
//            if (tube.rects[0].location.z >= maxZ) {
//                tubes.remove(i);
//                continue;
//            }
//            if (tube.clamped) {
//                for (int j = 0; j < tube.rects.length; j++) {
//                    PVector v1 = tube.rects[j].shape.getVertex(0);
//                    PVector v2 = tube.rects[j].shape.getVertex(1);
//                    v1.z += speed * (inTime.get() - prevManageTime) / 1000.0f;
//                    v2.z += speed * (inTime.get() - prevManageTime) / 1000.0f;
//                    tube.rects[j].shape.setVertex(0, v1);
//                    tube.rects[j].shape.setVertex(1, v2);
//                }
//            } else {
//                for (int j = 0; j < tube.rects.length; j++) {
//                    tube.rects[j].shape.translate(0, 0, speed * (inTime.get() - prevManageTime) / 1000.0f);
//                    //updating the z marked location of this vertex
//                    tube.rects[j].location.z += speed * (inTime.get() - prevManageTime) / 1000.0f;//this is getting silly
//                }
//            }
//            /*
//             //should translate the shapes and not move the poitns and recreate the rects
//             //dis bayd
//             if (tube.back.points[0].z >= maxZ) {
//             tubes.remove(i);
//             continue;
//             }
//             for (Point p : tube.front.points) {
//             p.z += speed * (inTime.get() - prevManageTime) / 1000.0;
//             }
//             if (!tube.clamped) {
//             for (Point p : tube.back.points) {
//             p.z += speed * (inTime.get() - prevManageTime) / 1000.0;
//             }
//             }
//             tube.createRects(tube.front, tube.back);
//             */
//        }
//        prevManageTime = inTime.get();
//
//    }
//
//    public void draw() {
//        ArrayList<MyTube> tubes = outTubes.get();
//        int len = tubes.size();
//        for (int i = 0; i < len; i++) {
//            tubes.get(i).draw();
//        }
//    }
//    
//    
//
//    private MyTube getLastTube() {
//        return internalTubesArray[tubeArrayCount];
//    }
//    
//    private void addTube(MyTube t) {
//        internalTubesArray[tubeArrayCount] = t;
//        tubeArrayCount++;        
//    }
//    
//    private void remove(int tubeIndex) {
//        internalTubesArray[tubeIndex] = null;
//        tubeArrayCount++;        
//    }
//
//    public static enum TubeCreationType {
//
//        SINGLE, CONTINIOUS, REPEATED, NONE;
//    }
//}
