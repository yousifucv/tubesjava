/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modules.flowcontrol;

import java.util.ArrayList;
import modules.ExecutableContainer;
import modules.ExecutableContainer;
import ports.InPort;
import ports.InterfacePort;
import ports.OutPort;
import ports.Port;
import tubesjava.IExecutable;

/**
 *
 * @author Yousif
 */
public class Swtich<T> implements IExecutable {

    public InPort<T> toBeEquatedTo;
    public ArrayList<OutPort> outPorts;
    private ArrayList<CaseCondition> cases;
    private ArrayList<ExecutableContainer> caseBlocks;

    public Swtich(InPort<T> in) {
        toBeEquatedTo = in;
    }

    public void createCase() {
        CaseCondition caseCheck = new CaseCondition(this.toBeEquatedTo);
        ExecutableContainer caseBlock = new ExecutableContainer();
        cases.add(caseCheck);
        caseBlocks.add(caseBlock);
    }

    public void createCase(CaseCondition c, CaseBlock ec) {
        cases.add(c);
        caseBlocks.add(ec);
    }

    @Override
    public void execute() {
        T input = toBeEquatedTo.get();
        int length = cases.size();
        for (int i = 0; i < length; i++) {
            CaseCondition c = cases.get(i);
//            c.comparatee.set(input);
            c.execute();
            if (c.getResult()) {
                caseBlocks.get(i).execute();
                break; //break out on the first match, NO FALL THROUGH IMPLEMENTATION
            }
        }
    }

    public class CaseCondition<T> extends ExecutableContainer {

        InPort<T> value;
        InterfacePort<T> comparatee;
        Boolean result;

        public CaseCondition(InPort<T> value) {
            this.value = value;
//            comparatee = new InterfacePort<>();
        }
        
        @Override
        public void execute() {
            super.execute();
            result = comparatee.get().equals(null);
        }

        public Boolean getResult() {
            return result;
        }
    }

    public class CaseBlock extends ExecutableContainer {

        public ArrayList<InterfacePort[]> portPairs;

        public CaseBlock() {
            portPairs = new ArrayList<>();
        }
        
        @Override
        public void execute() {
            super.execute();
            int length = portPairs.size();
            for(int i = 0; i < length; i++) {
                portPairs.get(i)[1].out = portPairs.get(i)[0].out;
            }
        }
        
        public void addConnection(InterfacePort in, InterfacePort out) {
            InterfacePort[] p = new InterfacePort[2];
            p[0] = in;
            p[1] = out;
            portPairs.add(p);
        }
    }
}
