/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modules;

import ports.InPort;
import ports.OutPort;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import tubesjava.IExecutable;
import tubesjava.MyKeyEvent;

/**
 *
 * @author Yousif
 */
public class Module_KeyBoard implements IExecutable {

    public InPort<LinkedList<MyKeyEvent>> inKey;
    int size = 1024;
    public OutPort<ArrayList<Boolean>> keyMapping;

    public Module_KeyBoard() {
        inKey = new InPort<>();                
        keyMapping = new OutPort();
        keyMapping.set(new ArrayList<Boolean>(size));
        for (int i = 0; i < size; i++) {
            keyMapping.get().add(Boolean.FALSE);
        }
    }

    @Override
    public void execute() {
        while(!inKey.get().isEmpty()) {
            MyKeyEvent key = inKey.get().poll();
            if (key.type == KeyEvent.KEY_PRESSED) {
                keyMapping.get().set(key.keyCode, Boolean.TRUE);
            } else if (key.type == KeyEvent.KEY_RELEASED){
                keyMapping.get().set(key.keyCode, Boolean.FALSE);
            }
        }
    }
}
