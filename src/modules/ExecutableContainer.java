/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modules;

import java.util.ArrayList;
import ports.InterfacePort;
import ports.InterfacePortable;
import tubesjava.IExecutable;

/**
 *
 * @author Yousif
 */
public class ExecutableContainer implements InterfacePortable, IExecutable {
    
    ArrayList<IExecutable> executables;
    ArrayList<InterfacePort> interfacePorts;
    
    public ExecutableContainer() {
        interfacePorts = new ArrayList<>();
        executables = new ArrayList<>();
    }
    
    public void add(IExecutable exec) {
        executables.add(exec);
    }
    

    @Override
    public void addInterfacePort(InterfacePort i) {
        interfacePorts.add(i);
    }

    @Override
    public void removeInterfacePort(InterfacePort i) {
        interfacePorts.remove(i);
    }

    @Override
    public void execute() {
        int length = executables.size();
        for(int i = 0; i < length; i++) {
            executables.get(i).execute();
        }
    }
}
