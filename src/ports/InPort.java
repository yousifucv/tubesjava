/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ports;

import ports.OutPort;

/**
 *
 * @author Yousif
 */
public class InPort<T> extends Port<T> {

    public T value;
    private Boolean connected = false;
    public Port<T> output = null;

    public InPort() {
    }

    public InPort(T val) {
        this();
        if (val != null) {
            type = val.getClass().toString();
            this.value = val;
        }
    }

    public void set(T val) {
        this.value = val;
    }

    @Override
    public T get() {
        if (connected) {
            return output.get();
        } else {
            return value;
        }
    }

    public void connectTo(OutPort<T> port) {
        output = port;
        connected = true;
    }

    public void connectTo(InterfacePort<T> port) {
        output = port;
        connected = true;
    }

    public void disconnect() {
        output = null;
        connected = false;
    }
}
