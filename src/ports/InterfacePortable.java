/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ports;

/**
 *
 * @author Yousif
 */
public interface InterfacePortable {
    
    public void addInterfacePort(InterfacePort i);
    
    public void removeInterfacePort(InterfacePort i);
    
}
