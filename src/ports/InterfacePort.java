/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ports;

import tubesjava.IExecutable;

/**
 *
 * @author Yousif
 */
public class InterfacePort<T> extends Port implements IExecutable {

    public InPort<T> in;
    public OutPort<T> out;
    
    public InterfacePort(InPort<T> in) {
        type = in.type;
        this.in = in;
    } 
    
    public InterfacePort(OutPort<T> out) {
        type = in.type;
        this.out = out;
    } 
    
    
    public InterfacePort(InPort<T> in, OutPort<T> out) {
        this.in = in;
        this.out = out;
    }    
    
    public void connectTo(OutPort<T> out) {
        if (this.in != null) {
            this.in.connectTo(out);
        } else {
            throw new RuntimeException("Should supply an out port"); //should be Exception but lazy
        }
    }
    
    public T get() {
        if (out != null) {
            return out.get();
        } else {
            throw new RuntimeException("Can't "); //should be Exception but lazy
        }
    } 
    
   public void setPort(InterfacePort port) {
       
   }

    @Override
    public void execute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
