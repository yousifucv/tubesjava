/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ports;

/**
 *
 * @author Yousif
 */
public class OutPort<T> extends Port<T> {

    public T value;

    public OutPort() {
    }

    public OutPort(T val) {
        type = val.getClass().toString();
        this.value = val;
    }

    public void set(T val) {
        this.value = val;
    }

    @Override
    public T get() {
        return value;
    }
}