/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drafts;

import java.util.ArrayList;
import processing.core.*;
import main.P;
import main.PUtils;

/**
 *
 * @author Yousif
 */
public class FrameAlpha extends PApplet {

    public static void main(String args[]) {
        PApplet.main(new String[]{"--present", "drafts.FrameAlpha"});
    }
    FrameAlpha.FrameManager manager;
    int[] colors = new int[3];
    int minimumKeyTime = 5;
    int previousTime;
    int bg = color(0);
    int blastColorIndex;

    @Override
    public void setup() {
        size(displayWidth - 50, displayHeight - 50);//, P3D);
        manager = new FrameAlpha.FrameManager();
        previousTime = millis();
        colors[0] = 0xFF3399;
        colors[1] = 0xFFFF00;
        colors[2] = 0x0099FF;
        blastColorIndex = 0;
    }

    @Override
    public void draw() {
        background(bg);

        manager.draw();

        bg = color(PUtils.red(bg) * 0.95f, PUtils.green(bg) * 0.95f, PUtils.blue(bg) * 0.95f);
    }

    @Override
    public void keyPressed() {
        if ((millis() - previousTime) > minimumKeyTime) {
            FrameAlpha.FrameAttributes attr;
            FrameAlpha.Frame f;
            FrameAlpha.FrameAnimation fa;
            switch (key) {
                case 'p':
                    attr = new FrameAlpha.FrameAttributes(50, colors[0]);
                    f = new FrameAlpha.Frame(width, height, attr);
                    fa = new FrameAlpha.FrameAnimation(f);
                    manager.addFrame(fa);
                    break;
                case 'o':
                    attr = new FrameAlpha.FrameAttributes(50, colors[1]);
                    f = new FrameAlpha.Frame(width, height, attr);
                    fa = new FrameAlpha.FrameAnimation(f);
                    manager.addFrame(fa);
                    break;
                case 'i':
                    attr = new FrameAlpha.FrameAttributes(50, colors[2]);
                    f = new FrameAlpha.Frame(width, height, attr);
                    fa = new FrameAlpha.FrameAnimation(f);
                    manager.addFrame(fa);
                    break;
                case 'q':
                    attr = new FrameAlpha.FrameAttributes(50, color(200, 200, 200));
                    f = new FrameAlpha.Frame(width, height, attr);
                    fa = new FrameAlpha.FrameAnimation(f);
                    manager.addFrame(fa);
                    break;
            }
            previousTime = millis();
        }
        if (key == 'z') {
            bg = color(255);
        } else if (key == 'x') {
            bg = colors[blastColorIndex];
        } else if (key == 'c') {
            blastColorIndex = (++blastColorIndex) % 3;
        }
    }

//--------FRAME MANAGER CLASS----
    static class FrameManager {

        ArrayList<FrameAlpha.FrameAnimation> framesList;

        void setup() {
        }

        FrameManager() {
            framesList = new ArrayList<>();
        }

        void addFrame(FrameAlpha.FrameAnimation f) {
            framesList.add(f);
        }

        void draw() {
            for (int i = 0; i < framesList.size(); i++) {
                FrameAlpha.FrameAnimation current = framesList.get(i);

                current.draw();
                if (current.isDone()) {
                    framesList.remove(i);
                }
            }
        }
    }
//--------END FRAME MANAGER CLASS----

//---------FRAME CLASS-----------
    class Frame {

        int frameWidth, frameHeight;
        FrameAlpha.FrameAttributes frameAttr;

        void setup() {
        }

        Frame(int initialWidth, int initialHeight, FrameAlpha.FrameAttributes attr) {
            this.frameWidth = initialWidth;
            this.frameHeight = initialHeight;
            this.frameAttr = attr;
        }

        void draw() {
            int upperLeftX = (width - frameWidth) / 2;
            int upperLeftY = (height - frameHeight) / 2;
            noFill();
            strokeWeight(this.frameAttr.thickness);
            stroke(this.frameAttr.frameColor);
//            PShape p = createShape(RECT, upperLeftX, upperLeftY, frameWidth, frameHeight);
//            shape(p ,0 ,0);
            rect(upperLeftX, upperLeftY, frameWidth, frameHeight);
        }
    }
//---------END FRAME CLASS-----------

//---------FRAME ATTRIBUTES CLASS-----------
    class FrameAttributes {

        float thickness;
        int frameColor;

        void setup() {
        }

        FrameAttributes(float thickness, int col) {
            this.thickness = thickness;
            this.frameColor = col;
        }

        void draw() {
        }
    }
//---------END FRAME ATTRIBUTES CLASS-----------

//---------FRAME ANIMATION CLASS-----------
    class FrameAnimation {

        FrameAlpha.Frame frame;
        int speed;

        FrameAnimation(FrameAlpha.Frame f) {
            this.frame = f;
            this.speed = 1;
        }

        void draw() {
            frame.draw();
            frame.frameWidth *= 1.00 - speed / 100.0;
            frame.frameHeight *= 1.00 - speed / 100.0;
            frame.frameAttr.thickness *= 0.98;
            int c = frame.frameAttr.frameColor;
            c = color(PUtils.red(c) * 0.99f, PUtils.green(c) * 0.99f, PUtils.blue(c) * 0.99f);
            //       c = color(PUtils.red(c),PUtils.green(c),PUtils.blue(c),alpha(c)*0.99);
            frame.frameAttr.frameColor = c;
        }

        boolean isDone() {
            return (frame.frameWidth < 0.0001 && frame.frameHeight < 0.0001);
        }
    }
//---------END FRAME ANIMATION CLASS-----------
//     public void setup() {
//        size(displayWidth, displayHeight);
//        background(0);
//    }
//
//    public void draw() {
//        background(0);
//        stroke(255);
//        if (mousePressed) {
//            line(mouseX, mouseY, pmouseX, pmouseY);
//        }
//    }
}
