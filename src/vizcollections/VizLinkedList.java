/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vizcollections;

import tubesjava.MyTube;

/**
 * Copied and simplified from Java's Collections API
 *
 * @author Yousif
 */
public class VizLinkedList<E> {

    int size;
    public Node<E> firstNode;
    public Node<E> lastNode;

    public VizLinkedList() {
    }

    public void add(E e) {
        final Node<E> l = lastNode;
        final Node<E> newNode = new Node<>(l, e, null);
        lastNode = newNode;
        if (l == null) {
            firstNode = newNode;
        } else {
            l.next = newNode;
        }
        size++;
    }

    public void remove(Node<E> node) {
//        final E element = node.item;
        final Node<E> next = node.next;
        final Node<E> prev = node.prev;

        if (prev == null) {
            firstNode = next;
        } else {
            prev.next = next;
            node.prev = null;
        }

        if (next == null) {
            lastNode = prev;
        } else {
            next.prev = prev;
            node.next = null;
        }

        node.item = null;
        size--;
    }

    public E getFirst() {
        final Node<E> f = firstNode;
        return f.item;
    }

    public E getLast() {
        final Node<E> l = lastNode;
        return l.item;
    }

    public int size() {
        return size;
    }
    
    public int exploreSize() {
        int result = 0;
        Node<E> node = firstNode;
//        for (tubeNode = tubes.firstNode; tubeNode != null; tubeNode = tubeNode.next()) {
        while (node != null) {
            result++;
            node = node.next();
        }
        return result;
    }

    public class Node<E> {

        E item;
        Node<E> next;
        Node<E> prev;

        private Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }

        public E getItem() {
            return item;
        }

        public Node<E> next() {
            return next;
        }
    }
}
